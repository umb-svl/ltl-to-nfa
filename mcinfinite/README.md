# Model checking NFAs with FLTL in the infinite domain


![https://github.com/psf/black](https://img.shields.io/badge/code%20style-black-000000.svg)

### Building the project

`make init`

### Spawning a bash environment

`make`

The commands above use docker which is the recommended way of using this project. 
The poetry.lock file is also mounted in order to be able to commit this file after updating.


### Run the tool

```shell
# show all options
mcinfinite -h

# convert NFA+claims into a NuSMV model and model check it
mcinfinite examples/integration_vhandler-int-nfa.scy examples/integration_vhandler-int-claims.claims
```

### Setting Docker with PyCharm

[Debugging a Containerized Django App in PyCharm](https://testdriven.io/blog/django-debugging-pycharm).


### How to debug using PyCharm and Docker


![media/docker-debug-configuration.png](media/docker-debug-configuration.png)

### Useful poetry commands

````shell
 # show env info (useful for configuring your preferred IDE)
 # Example configurations for PyCharm: https://www.reddit.com/r/pycharm/comments/elga2z/using_pycharm_for_poetrybased_projects/
 poetry env info


 # these are all equivalent
 poetry run python -m nfa2nusmv
 poetry run nfa2nusmv
 nfa2nusmv
```` 

# Manual Installation

---
**WARNING**

You will need to install [poetry](https://python-poetry.org/docs/#installation) and the graphviz dot tool (for 
visualizing examples). Make sure you install poetry with the appropriate Python version (>=3.10). For more info on 
how to install poetry with pyenv [follow this link](https://python-poetry.org/docs/managing-environments/).

You will also need to install NuSMV.

---

### To install the tool

`poetry install`

### To install NuSMV on macOS

`brew install nu-smv`
