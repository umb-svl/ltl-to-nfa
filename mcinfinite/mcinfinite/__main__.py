import logging

import sys
import yaml
import argparse
import subprocess
from pathlib import Path
from dataclasses import dataclass, field
from typing import List, Mapping, Optional

from mcinfinite import modelchecker


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("modelchecker")
VERBOSE: bool = False


def parse_command():
    parser = argparse.ArgumentParser()
    parser.add_argument("nfa", type=Path, help="NFA file path.")
    parser.add_argument("claims", type=Path, help="LTL file path")
    parser.add_argument(
        "-v", "--verbosity", help="increase output verbosity", action="store_true"
    )

    return parser.parse_args()


def main():
    global VERBOSE
    args = parse_command()

    if args.verbosity:
        VERBOSE = True
        logger.setLevel(logging.DEBUG)

    logger.debug(f"Current path: {Path.cwd()}")

    nfa_path: Path = args.nfa
    claims_path: Path = args.claims
    model_path: Path = nfa_path.parent / (nfa_path.stem + ".smv")

    logger.debug(f"NFA path: {nfa_path}")
    logger.debug(f"Claims path: {claims_path}")
    logger.debug(f"NuSMV model path: {model_path}")

    modelchecker.run_mc(nfa_path, claims_path, model_path)


if __name__ == "__main__":
    main()
