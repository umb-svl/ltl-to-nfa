from typing import List, Union, Optional, Tuple, IO
from pathlib import Path
from dataclasses import dataclass, field

from mcinfinite.parsers import ltlf_lark_parser
import mcinfinite.parsers.ltlf_lark_parser
from mcinfinite.parsers.ltlf_lark_parser import (
    Variable,
    And,
    Equal,
    Action,
    NotAction,
    Not,
    Always,
    Next,
    Implies,
    Or,
    Until,
    Bool,
    Eventually,
    ExistsFinally,
    Formula,
    EndOfSequence,
    UnaryOperator,
    BinaryOperator,
    Atomic,    
    LTL,
    LTL_F,
    CTL,
    EOS,
)
from io import StringIO

@dataclass
class Op:
    targets: list
    is_final: bool
    is_initial: bool

    @classmethod
    def make(cls, is_final=False, is_initial=False):
        return cls(targets=[], is_final=is_final, is_initial=is_initial)

    def add(self, name):
        self.targets.append(name)


@dataclass
class Spec:
    formulae: List[Union[CTL, LTL, LTL_F]] = field(default_factory=list)
    comment: Optional[str] = field(default=None)

    def add(self, formula):
        self.formulae.append(formula)

    def __len__(self):
        return len(self.formulae)

    def dump(
        self, fp, action_name: Optional[str] = None, eos_name: Optional[str] = None
    ):
        action = Variable("_action" if action_name is None else action_name)
        eos = Variable("_eos" if eos_name is None else eos_name)
        if self.comment is not None:
            fp.write("-- ")
            fp.write(self.comment)
            fp.write("\n")

        for f in self.formulae:
            is_ctl = isinstance(f, CTL)
            f = ltlf_to_ltl(f, eos=eos, action=action)
            if is_ctl:
                fp.write("SPEC ")
            else:
                fp.write("LTLSPEC ")
            ltl_dump(formula=f, fp=fp)
            fp.write("\n")

    def dumps(self, action_name: Optional[str] = None, eos_name: Optional[str] = None):
        buffer = StringIO()
        self.dump(
            fp=buffer,
            action_name=action_name,
            eos_name=eos_name,
        )
        return buffer.getvalue()

def ltl_dump(formula: Formula, fp: IO, eos=None, nusvm_strict=True):
    """
    Converts a formula into an NuSMV string
    """

    def rec(formula):
        # Primitives
        if type(formula) in [LTL, CTL, LTL_F] and not nusvm_strict:
            formula = formula.formula
        if isinstance(formula, Bool):
            fp.write("TRUE" if formula.value else "FALSE")
        elif isinstance(formula, Variable):
            fp.write(formula.name)
        elif isinstance(formula, Action):
            if nusvm_strict:
                raise ValueError(formula)
            if formula.prefix is not None:
                fp.write(formula.prefix)
                fp.write("modelchecker")
            fp.write(formula.name)
        elif isinstance(formula, EndOfSequence):
            if nusvm_strict:
                raise ValueError(formula)
            fp.write(eos if eos is not None else "END")

        elif isinstance(formula, UnaryOperator):
            # Operator
            fp.write(formula.operator)
            fp.write(" ")
            # Child
            if isinstance(formula.child, Atomic):
                rec(formula.child)
            else:
                fp.write("(")
                rec(formula.child)
                fp.write(")")

        elif isinstance(formula, BinaryOperator):
            # Left
            if isinstance(formula.left, Atomic):
                rec(formula.left)
            else:
                fp.write("(")
                rec(formula.left)
                fp.write(")")
            # Operator
            fp.write(" ")
            fp.write(formula.operator)
            fp.write(" ")
            # Right
            if isinstance(formula.right, Atomic):
                rec(formula.right)
            else:
                fp.write("(")
                rec(formula.right)
                fp.write(")")
        else:
            raise ValueError(type(formula), formula)

    # Recursive call
    rec(formula)


def dumps(formula, eos=None, nusvm_strict=True):
    buffer = StringIO()
    ltl_dump(
        formula=formula,
        fp=buffer,
        eos=eos,
        nusvm_strict=nusvm_strict,
    )
    return buffer.getvalue()

# Conversions


def ltlf_to_ltl(
    formula: Formula, eos: Variable, action: Variable, prefix_separator="_"
) -> Formula:
    """
    Converts an LTLf formula in an LTL formula
    """

    def rec(formula, mode=LTL_F):
        while type(formula) in (LTL, CTL, LTL_F):
            mode = type(formula)
            formula = formula.formula

        if mode is LTL or mode is CTL:
            if isinstance(formula, Atomic):
                if not (isinstance(formula, Bool) or isinstance(formula, Variable)):
                    raise ValueError(
                        f"Expecting an atomic {mode.__name__} formula, but got: ",
                        formula,
                    )
                return formula
            elif isinstance(formula, UnaryOperator):
                return type(formula)(rec(formula.child, mode=mode))
            elif isinstance(formula, BinaryOperator):
                return type(formula)(
                    rec(formula.left, mode=mode), rec(formula.right, mode=mode)
                )

        assert mode is LTL_F

        if isinstance(formula, Bool) or isinstance(formula, Variable):
            return formula
        elif isinstance(formula, NotAction) or isinstance(formula, Action):
            if formula.prefix is None:
                name = formula.name
            else:
                name = formula.prefix + prefix_separator + formula.name
            act = Variable(name)
            return And(
                Not(act) if isinstance(formula, NotAction) else act, Not(rec(EOS))
            )
        elif isinstance(formula, EndOfSequence):
            return eos
        elif isinstance(formula, Not):
            return Not(rec(formula.child))
        elif isinstance(formula, Or):
            return Or(rec(formula.left), rec(formula.right))
        elif isinstance(formula, Equal):
            return Equal(rec(formula.left), rec(formula.right))
        elif isinstance(formula, And):
            return And(rec(formula.left), rec(formula.right))
        elif isinstance(formula, Implies):
            return Implies(rec(formula.left), rec(formula.right))
        elif isinstance(formula, Next):
            return Next(And(rec(formula.child), Not(rec(EOS))))
        elif isinstance(formula, Eventually):
            return Eventually(And(rec(formula.child), Not(rec(EOS))))
        elif isinstance(formula, Always):
            return Always(Or(rec(formula.child), rec(EOS)))
        elif isinstance(formula, Until):
            return Until(
                left=rec(formula.left), right=And(rec(formula.right), Not(rec(EOS)))
            )
        else:
            raise ValueError(type(formula), formula, mode)

    return rec(formula)