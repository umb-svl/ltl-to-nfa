from pathlib import Path
from typing import Any, Optional
import yaml
from karakuri import regular
from typing import Dict
from mcinfinite.fsm import FSM

def fsm2smv(
    fsm_model: Path,
    smv_model: Path,
    project_prefix: Optional[str] = None
) -> None:
    """
    Convert an FSM model (.scy) to an SMV model (.smv).
    @param fsm_model: path to the input .scy file
    @param smv_model: path to the output .smv file
    @param filter_instance:
    """

    fsm = FSM(fsm_model).no_sink().to_dfa().no_empty().to_nfa()

    with smv_model.open("w") as fp:
        boolean_alphabet = smv_dump(state_diagram=fsm.as_dict(), alphabet=fsm.raw_alphabet, fp=fp)

    return boolean_alphabet

# LTLSPEC (action=level1) -> (action=standby1 | action=level1);
def smv_dump(
    state_diagram,
    alphabet,
    fp,
    var_eos: str = "_eos",
    var_state: str = "_state"
) -> None:
    INDENT = "    "

    def render_values(elem):
        if isinstance(elem, str):
            return elem
        if len(elem) == 0:
            raise ValueError()
        return "{" + ", ".join(map(str, elem)) + "}"

    def decl_var(name, values):
        print(f"{INDENT}{name}: {render_values(values)};", file=fp)

    def add_edge(src, char, dst):
        act = f" & {char}" if char is not None else ""
        print(f"{var_state}={src}{act}: {dst};", file=fp)

    def init_var(name, values):
        print(f"{INDENT}init({name}) := {render_values(values)};", file=fp)

    def next_var_case(variable, elems):
        print(f"{INDENT}next({variable}) := case", file=fp)
        for (cond, res) in elems:
            res = render_values(res)
            print(f"{INDENT}{INDENT}{cond} : {res};", file=fp)
        print(f"{INDENT}esac;", file=fp)

    def infer_boolean_alphabet(alphabet: Dict[str, Dict[str, bool]]):
        """
        Original YAML:
            alphabet:
                c1: {a: false, b: false} # check first to retrieve variables (the real alphabet is 'a' and 'b')
                c2: {a: false, b: true}
                c3: {a: true, b: false}
                c4: {a: true, b: true}
            edges:
                (...)
        @param alphabet: the alphabet entry as a dict
            Example: {'c1': {'a': False, 'b': False}, 'c2': {'a': False, 'b': True}, 'c3': {'a': True, 'b': False}, 'c4': {'a': True, 'b': True}}
        @return: List of strings. The real alphabet of strings to booleans.
            Example: ['a', 'b']
        """
        first_char : Dict[str, bool] = list(alphabet.values())[0] # any char is valid here, doesn't matter
        return list(first_char.keys())

    def char_to_boolean_expr(char: str, alphabet: Dict[str, Dict[str, bool]]):
        variables_dict : Dict[str, bool] = alphabet[char]
        boolean_expr = ""
        for key, value in variables_dict.items():
            boolean_expr += f"{key} & "  if value is True else f"!{key} & "
        return boolean_expr[:-3]

    boolean_alphabet = infer_boolean_alphabet(alphabet)


    print("MODULE main", file=fp)
    print("VAR", file=fp)
    decl_var(f"{var_eos}", "boolean")

    for char in boolean_alphabet:
        decl_var(f"{char}", "boolean")

    states = list(
        set(x["src"] for x in state_diagram["edges"]).union(
            set(x["dst"] for x in state_diagram["edges"])
        )
    )
    states.sort()

    decl_var(f"{var_state}", states)

    print("ASSIGN", file=fp)

    # State
    init_var(f"{var_state}", [state_diagram["start_state"]])

    print(f"{INDENT}next({var_state}) := case", file=fp)
    print(
        f"{INDENT}{INDENT}{var_eos}: {var_state}; -- finished, no change in state",
        file=fp,
    )

    for edge in state_diagram["edges"]:
        print(f"{INDENT}{INDENT}", end="", file=fp)
        src, char, dst = edge["src"], char_to_boolean_expr(edge["char"], alphabet), edge["dst"]
        add_edge(src, char, dst)
    print(f"{INDENT}esac;", file=fp)

    # Boolean alphabet
    for char in boolean_alphabet:
        print(f"{INDENT}-- generate {char}\n", end="", file=fp)
        init_var(char,["TRUE", "FALSE"])
        lines = [(var_eos, char), ("TRUE", ["TRUE", "FALSE"])]
        next_var_case(char, lines)

    # EOS (Final state)
    print(f"{INDENT}-- generate final state\n", end="", file=fp)
    init_var(var_eos,["FALSE"])
    lines = [(var_eos, "TRUE")]
    for edge in state_diagram["edges"]:
        src, char, dst = edge["src"], char_to_boolean_expr(edge["char"], alphabet), edge["dst"]
        if dst in state_diagram["accepted_states"]:
            act = f" & {char}" if char is not None else ""
            lines.append((f"{var_state}={src}{act}", ["TRUE", "FALSE"]))
    lines.append(("TRUE", "FALSE"))
    next_var_case(var_eos, lines)

    print(f"\nFAIRNESS {var_eos};", file=fp)

    return boolean_alphabet