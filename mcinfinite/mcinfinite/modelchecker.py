import logging

import sys
import subprocess
from pathlib import Path
from dataclasses import dataclass, field
from typing import List

from mcinfinite.parsers import ltlf_lark_parser
from mcinfinite.parsers.ltlf_lark_parser import Formula, LTL_F
from mcinfinite.ltlf import Spec
from mcinfinite import ltlf
from mcinfinite import fsm2smv

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("modelchecker")
VERBOSE: bool = False


def model_check(smv_path: Path, boolean_alphabet: List[str]):
    try:
        nusmv_call = [
            "NuSMV",
            str(smv_path),
        ]
        logger.debug(" ".join(nusmv_call))
        cp: subprocess.CompletedProcess = subprocess.run(
            nusmv_call, capture_output=True, check=True
        )
        return check_nusmv_output(cp.stdout.decode(), boolean_alphabet)
    except subprocess.CalledProcessError as err:
        print(err.output.decode() + err.stderr.decode())
        sys.exit(255)


def parse_states(trace):
    state = dict()
    result = []
    pending = False
    for line in trace:
        if line.startswith("-> State"):
            if len(state) == 0:
                continue
            result.append(state)
            state = dict(state)
            pending = True
        elif line.startswith("-- Loop"):
            continue
        else:
            k, v = line.split(" = ", 1)
            state[k.strip()] = v.strip()
            pending = True
    if pending:
        result.append(state)
    return result


def parse_trace(trace, boolean_alphabet: List[str]):
    actions = []
    for state in parse_states(trace):
        if state["_state"] == "-1":
            continue
        if state["_eos"] == "TRUE":
            break
        boolean_expr = ""
        for char in boolean_alphabet:
            boolean_expr += f"{char} & " if state[char] == 'TRUE' else f"!{char} & "
        actions.append(f"{boolean_expr[:-3]} (t?)")
    return actions


def check_nusmv_output(raw_output: str, boolean_alphabet: List[str]):
    lines_output: List[str] = raw_output.splitlines()
    error: bool = False
    specs = []
    results = []
    handle_trace = False
    trace = []
    for line in lines_output:
        if line.startswith("Trace ") or line.startswith("-- as demonstrated by "):
            continue
        if handle_trace:
            if line.startswith("-- specification"):
                handle_trace = False
                results.append(parse_trace(trace, boolean_alphabet))
                trace = None
            else:
                trace.append(line.strip())
                continue
        if line.startswith("-- specification"):
            line = line[len("-- specification") :]
            line, answer = line.rsplit(" is ")
            result = answer.strip() == "true"
            specs.append(line.strip())
            if result:
                results.append(None)
            else:
                error = True
                handle_trace = True
                trace = []
    if len(specs) > len(results):
        results.append(parse_trace(trace, boolean_alphabet))
    if error:
        logger.debug(raw_output)
    if error:
        return list(zip(specs, results))
    else:
        return None


@dataclass
class ModelChecker:
    file: Path
    specs: List[Spec] = field(default_factory=list)

    def add(self, spec: Spec):
        self.specs.append(spec)

    def __len__(self):
        return sum(len(s) for s in self.specs)

    def run(self, boolean_alphabet: List[str]):
        count = len(self)
        if count == 0:
            logger.debug(f"Skip model checking {self.file} (0 formulas)")
            return
        with self.file.open("a+") as fp:
            for spec in self.specs:
                logger.debug(spec)
                spec.dump(fp)
        result = model_check(self.file, boolean_alphabet)
        if result is not None:
            specs = []
            for s in self.specs:  # [::-1]:  # NuSMV lists CTL before LTL
                for f in s.formulae:
                    specs.append((f, s.comment))
            for ((formula, comment), (raw_formula, trace)) in zip(specs, result):
                if trace is not None:
                    print("Error in specification:", comment, file=sys.stderr)
                    print(
                        "Formula:",
                        ltlf.dumps(formula, nusvm_strict=False),
                        file=sys.stderr,
                    )
                    trace_str = "; ".join(trace) if len(trace) > 0 else "(empty)"
                    print("Counter example:", trace_str, file=sys.stderr)
                    sys.exit(255)

def run_mc(nfa_path: Path, claims_path: Path, model_path: Path) -> None:
    mc = ModelChecker(model_path)

    ltlf_formulas: List[Formula] = ltlf_lark_parser.parse(claims_path)

    spec = Spec(formulae=[], comment="USER CLAIMS")

    for entry in ltlf_formulas:
        logger.debug(f"Appending LTLf formula: {entry}")
        spec.formulae.append(LTL_F(entry))

    mc.add(spec)

    if len(mc) > 0:
        # Create the integration SMV
        logger.debug(f"Creating model: {model_path}")
        boolean_alphabet = fsm2smv.fsm2smv(nfa_path, model_path)
        logger.debug("Model checking integration...")
        mc.run(boolean_alphabet)
    else:
        logger.debug(f"Skip model checking (0 formulas)")
