from lark import Lark, Transformer
from dataclasses import dataclass, field
from io import StringIO
from typing import List, Optional
from pathlib import Path

@dataclass
class Formula:
    pass


@dataclass
class Atomic(Formula):
    pass


@dataclass
class BinaryOperator:
    left: Formula
    right: Formula


@dataclass
class UnaryOperator:
    child: Formula


@dataclass
class CTL(Formula):
    formula: Formula


@dataclass
class LTL(Formula):
    formula: Formula


@dataclass
class LTL_F(Formula):
    formula: Formula


def fold(op, zero, args):
    result = None
    for arg in args:
        if result is None:
            result = arg
        else:
            result = op(result, arg)
    if result is None:
        return zero
    else:
        return result


# Atoms


@dataclass
class Bool(Atomic):
    value: bool


@dataclass
class Variable(Atomic):
    name: str


@dataclass
class Action(Atomic):
    name: str
    prefix: Optional[str] = field(default=None)


@dataclass
class NotAction(Atomic):
    name: str
    prefix: Optional[str] = field(default=None)


# Specific to LTLF


@dataclass
class EndOfSequence(Atomic):
    pass


EOS = EndOfSequence()

# Propositional logic


@dataclass
class Not(UnaryOperator):
    operator = "!"


@dataclass
class And(BinaryOperator):
    operator = "&"

    @classmethod
    def make(cls, args):
        return fold(cls, Bool(True), args)


@dataclass
class Equal(BinaryOperator):
    operator = "="


@dataclass
class Or(BinaryOperator):
    operator = "|"

    @classmethod
    def make(cls, args):
        return fold(cls, Bool(True), args)


@dataclass
class Implies(BinaryOperator):
    operator = "->"


# Linear Temporal Logic


@dataclass
class Next(UnaryOperator):
    "Must be true in the following step."
    operator = "X"


@dataclass
class Eventually(UnaryOperator):
    "The formula will eventually hold"
    operator = "F"


@dataclass
class Always(UnaryOperator):
    "The formula holds at every step of the trace"
    operator = "G"


@dataclass
class Until(BinaryOperator):
    "Left must happen until right happens; right will eventually happen."
    operator = "U"


# CTL


@dataclass
class ExistsFinally(UnaryOperator):
    """
    p is true in a state s0 if there exists a series of transitions
    s_0 -> s_1, s_1 -> s_2, ...,s_{n−1} -> s_n such that p is true in s_n

    TODO: THIS IS A CTL FORMULA RATHER THAN LTLf!
    """

    operator = "EF"


# Abbreviations


def Last():
    "The last instance of a trace"
    return Not(Next(Bool(True)))


def WeakNext(formula):
    "The given formula must hold unless it is last."
    return Not(Next(Not(formula)))


def Equiv(left, right):
    "Logical equivalence"
    return And(Implies(left, right), Implies(right, left))


def Releases(left, right):
    "If left never becomes true, right must remain true forever."
    return Not(Until(Not(left), Not(right)))


def WeakUntil(left, right):
    "ψ has to hold at least until φ; if φ never becomes true, ψ must remain true forever. <=> φ R (φ ∨ ψ) <=> (ψ U φ) ∨ G ψ"
    # return Releases(right, Or(right, left))
    return Or(Until(left, right), Always(left))

parser = Lark.open("ltlf_grammar.lark", rel_to=__file__, start="claim_list")


class LTLParser(Transformer):
    def claim(self, args):
        return args[0]

    def claim_list(self, args):
        return args

    def end(self, args) -> Last:
        return Last()

    def true(self, args) -> Bool:
        return True

    def false(self, args) -> Bool:
        return False

    def bool(self, args) -> Bool:
        return Bool(args[0])

    def paren(self, args):
        return args[0]

    def land(self, args) -> And:
        return And(*args)

    def formula(self, args):
        return args[0]

    def lnext(self, args) -> Next:
        return Next(*args)

    def ident(self, args):
        return args[0].value

    def act(self, args) -> Action:
        if len(args) == 2:
            return Action(prefix=args[0], name=args[1])
        return Action(args[0])

    def lor(self, args) -> Or:
        return Or(*args)

    def lnot(self, args) -> Not:
        return Not(*args)

    def until(self, args) -> Until:
        return Until(*args)

    def releases(self, args) -> Releases:
        return Releases(*args)

    def wuntil(self, args) -> WeakUntil:
        return WeakUntil(*args)

    def implies(self, args) -> Implies:
        return Implies(*args)

    def equiv(self, args) -> Equiv:
        return Equiv(*args)

    def globally(self, args) -> Always:
        return Always(*args)

    def eventually(self, args) -> Eventually:
        return Eventually(*args)

    def last(self, args) -> Last:
        return Last()

    def eq(self, args) -> Equal:
        return Equal(*args)

    def atom(self, args):
        return args[0]

def parse(source: Path) -> Formula:
    with Path.open(source) as fp:
        tree = parser.parse(fp.read())
    return LTLParser().transform(tree)


def main():
    import sys

    if len(sys.argv) < 2:
        print("Please provide a valid source path! Usage: claim_parser PATH")
        sys.exit(255)
    print(parse(Path(sys.argv[1])))


if __name__ == "__main__":
    main()
