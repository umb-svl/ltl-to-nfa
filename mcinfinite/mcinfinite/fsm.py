from pathlib import Path
from typing import Any, Optional
import yaml
from karakuri import regular
from typing import Dict, List

class FSM:
    raw: Dict = None
    alphabet : List[str] = None
    raw_alphabet : Dict[str, Dict[str, bool]] = None

    def __init__(self, input: Path):
        """

        @param input:
        """
        with input.open("r") as fp:
            self.raw : Dict = yaml.load(fp, Loader=yaml.FullLoader)

        self.raw_alphabet = self.raw["alphabet"]
        self.alphabet = list(self.raw_alphabet.keys())

        self.reset()

    def reset(self):
        self._fsm = regular.NFA.from_dict(self.raw, self.alphabet)
        return self

    def value(self):
        return self._fsm

    def to_nfa(self):
        if isinstance(self._fsm, regular.DFA):
            self._fsm : regular.NFA[Any, str] = regular.dfa_to_nfa(self._fsm)
        return self

    def to_dfa(self, minimize=False):
        if isinstance(self._fsm, regular.NFA):
            self._fsm : regular.DFA[Any, str] = regular.nfa_to_dfa(self._fsm).flatten(minimize)
        return self

    def no_empty(self):
        if isinstance(self._fsm, regular.DFA):
            self._fsm = self._fsm.subtract(regular.DFA.make_nil(self.alphabet))
        else:
            print("Warning! Unsupported operation to NFA!")
        return self

    def no_epsilon(self):
        if isinstance(self._fsm, regular.NFA):
            self._fsm = self._fsm.remove_epsilon_transitions()
        else:
            print("Warning! Unsupported operation to DFA!")
        return self

    def no_sink(self):
        if isinstance(self._fsm, regular.NFA):
            self._fsm = self._fsm.remove_sink_states()
        else:
            print("Warning! Unsupported operation to DFA!")
        return self

    def as_dict(self, flatten=True):
        return self._fsm.as_dict(flatten=flatten)

    def export(self):
        pass # TODO:

# from mcinfinite.fsm import FSM
# from pathlib import Path
# input = Path('examples/figure5-v4.scy')
# f = FSM(input)

def fsm2smv(
    fsm_model: Path,
    smv_model: Path,
    project_prefix: Optional[str] = None
) -> None:
    """
    Convert an FSM model (.scy) to an SMV model (.smv).
    @param fsm_model: path to the input .scy file
    @param smv_model: path to the output .smv file
    @param filter_instance:
    """

    with fsm_model.open("r") as fp:
        fsm_dict = yaml.load(fp, Loader=yaml.FullLoader)

    alphabet = list(fsm_dict["alphabet"].keys())

    n: Optional[regular.NFA[Any, str]] = handle_fsm(
        regular.NFA.from_dict(fsm_dict, alphabet),
        dfa=True,
        dfa_no_empty_string=True,
        nfa_no_sink=True,
        project_prefix=project_prefix,
    )
    # Note: we could use fsm_stats.result_dfa but it would generate a "bigger" FSM
    # time(dfa2nfa)+time(nusmv(smaller_fsm)) vs time(nusmv(bigger_fsm))?

    # print(len(fsm_stats.result)) -> less states (also a DFA since dfa2nfa=dfa)
    # print(len(fsm_stats.result_dfa)) -> more states

    # # dump the .scy file for the submodule
    # if project_prefix is not None:
    #     subsystem_model = Path(f"{smv_model.stem}.scy")
    #     with subsystem_model.open("w") as fp:
    #         yaml.dump(n.as_dict(flatten=True), fp)

    with smv_model.open("w") as fp:
        boolean_alphabet = smv_dump(state_diagram=n.as_dict(flatten=True), alphabet=fsm_dict["alphabet"], fp=fp)

    return boolean_alphabet

# LTLSPEC (action=level1) -> (action=standby1 | action=level1);
def smv_dump(
    state_diagram,
    alphabet,
    fp,
    var_eos: str = "_eos",
    var_state: str = "_state"
) -> None:
    INDENT = "    "

    def render_values(elem):
        if isinstance(elem, str):
            return elem
        if len(elem) == 0:
            raise ValueError()
        return "{" + ", ".join(map(str, elem)) + "}"

    def decl_var(name, values):
        print(f"{INDENT}{name}: {render_values(values)};", file=fp)

    def add_edge(src, char, dst):
        act = f" & {char}" if char is not None else ""
        print(f"{var_state}={src}{act}: {dst};", file=fp)

    def init_var(name, values):
        print(f"{INDENT}init({name}) := {render_values(values)};", file=fp)

    def next_var_case(variable, elems):
        print(f"{INDENT}next({variable}) := case", file=fp)
        for (cond, res) in elems:
            res = render_values(res)
            print(f"{INDENT}{INDENT}{cond} : {res};", file=fp)
        print(f"{INDENT}esac;", file=fp)

    def infer_boolean_alphabet(alphabet: Dict[str, Dict[str, bool]]):
        """
        Original YAML:
            alphabet:
                c1: {a: false, b: false} # check first to retrieve variables (the real alphabet is 'a' and 'b')
                c2: {a: false, b: true}
                c3: {a: true, b: false}
                c4: {a: true, b: true}
            edges:
                (...)
        @param alphabet: the alphabet entry as a dict
            Example: {'c1': {'a': False, 'b': False}, 'c2': {'a': False, 'b': True}, 'c3': {'a': True, 'b': False}, 'c4': {'a': True, 'b': True}}
        @return: List of strings. The real alphabet of strings to booleans.
            Example: ['a', 'b']
        """
        first_char : Dict[str, bool] = list(alphabet.values())[0] # any char is valid here, doesn't matter
        return list(first_char.keys())

    def char_to_boolean_expr(char: str, alphabet: Dict[str, Dict[str, bool]]):
        variables_dict : Dict[str, bool] = alphabet[char]
        boolean_expr = ""
        for key, value in variables_dict.items():
            boolean_expr += f"{key} & "  if value is True else f"!{key} & "
        return boolean_expr[:-3]

    boolean_alphabet = infer_boolean_alphabet(alphabet)


    print("MODULE main", file=fp)
    print("VAR", file=fp)
    decl_var(f"{var_eos}", "boolean")

    for char in boolean_alphabet:
        decl_var(f"{char}", "boolean")

    print(state_diagram["edges"])
    states = list(
        set(x["src"] for x in state_diagram["edges"]).union(
            set(x["dst"] for x in state_diagram["edges"])
        )
    )
    states.sort()

    decl_var(f"{var_state}", states)

    print("ASSIGN", file=fp)

    # State
    init_var(f"{var_state}", [state_diagram["start_state"]])

    print(f"{INDENT}next({var_state}) := case", file=fp)
    print(
        f"{INDENT}{INDENT}{var_eos}: {var_state}; -- finished, no change in state",
        file=fp,
    )

    for edge in state_diagram["edges"]:
        print(f"{INDENT}{INDENT}", end="", file=fp)
        src, char, dst = edge["src"], char_to_boolean_expr(edge["char"], alphabet), edge["dst"]
        add_edge(src, char, dst)
    print(f"{INDENT}esac;", file=fp)

    # Boolean alphabet
    for char in boolean_alphabet:
        print(f"{INDENT}-- generate {char}\n", end="", file=fp)
        init_var(char,["TRUE", "FALSE"])
        lines = [(var_eos, char), ("TRUE", ["TRUE", "FALSE"])]
        next_var_case(char, lines)

    # EOS (Final state)
    print(f"{INDENT}-- generate final state\n", end="", file=fp)
    init_var(var_eos,["FALSE"])
    lines = [(var_eos, "TRUE")]
    for edge in state_diagram["edges"]:
        src, char, dst = edge["src"], char_to_boolean_expr(edge["char"], alphabet), edge["dst"]
        if dst in state_diagram["accepted_states"]:
            act = f" & {char}" if char is not None else ""
            lines.append((f"{var_state}={src}{act}", ["TRUE", "FALSE"]))
    lines.append(("TRUE", "FALSE"))
    next_var_case(var_eos, lines)

    print(f"\nFAIRNESS {var_eos};", file=fp)

    return boolean_alphabet

def handle_fsm(
    n: regular.NFA[Any, str],
    filter: str = None,
    dfa: bool = False,
    dfa_no_empty_string: bool = False,
    dfa_minimize: bool = False,
    nfa_no_sink: bool = False,  # set this to True for faster DFA minimization
    no_epsilon: bool = False,
    project_prefix: Optional[str] = None,
    dfa_no_sink: Optional[bool] = False,
) -> Optional[regular.NFA[Any, str]]:

    if project_prefix is not None:
        translate = dict()
        translate[None] = None
        offset = len(project_prefix)
        for name in n.alphabet:
            if name is not None and name.startswith(project_prefix):
                translate[name] = name[offset:]
            else:
                # Skip otherwise
                translate[name] = None
        n = n.map_alphabet(translate)

    if filter is not None:
        pattern = re.compile(filter)

        def on_elem(x):
            return x if x is None else pattern.match(x)

        n = n.filter_char(on_elem)

    if nfa_no_sink:
        # FASTER DFA MINIMIZATION
        # NOTE: If minimizing DFA, by removing sink states before there
        # is a unique sink state when we convert to DFA; this is a quick
        # way of making the resulting DFA smaller
        n = n.remove_sink_states()

    if no_epsilon:
        n = n.remove_epsilon_transitions()

    if dfa:
        d: regular.DFA[Any, str] = regular.nfa_to_dfa(n)

        if dfa_minimize:
            d = d.flatten(minimize=True)
        else:
            d = d.flatten()

        if dfa_no_empty_string:
            # Make sure that there is no empty string, this is important for LTLf
            # TODO: this option breaks the states' numbers
            d = d.subtract(regular.DFA.make_nil(d.alphabet))

        result = regular.dfa_to_nfa(d)

        if dfa_no_sink:
            result = fsm_stats.result.remove_sink_states()

    else:
        result = n

    return result
