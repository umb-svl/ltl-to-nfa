fn main() {
    println!("cargo:rerun-if-changed=src/ltl.lalrpop");
    lalrpop::process_root().unwrap();
}
