use std::fs::{self, File};
use std::io::prelude::*;

use automata::nfa::Nfa;

use ltl::ltl::PosNormal;
// use plotters::element::BackendCoordOnly;
// use plotters::prelude::*;

fn main() {
    // let mut buf = String::new();
    // let parser = ltl::parser::FormulaParser::new();

    // let entries = fs::read_dir("./samples")
    //     .expect("Couldn't find samples directory")
    //     .filter_map(|e| {
    //         let e = e.unwrap();
    //         if e.path().extension() == Some("ltl".as_ref()) {
    //             Some(e)
    //         } else {
    //             None
    //         }
    //     })
    //     .collect::<Vec<_>>();
    // let pnfs = entries
    //     .iter()
    //     .map(|e| {
    //         buf.clear();
    //         let mut f = fs::File::open(e.path()).expect("Couldn't open file");
    //         f.read_to_string(&mut buf).expect("Couldn't read file");
    //         (
    //             e,
    //             PosNormal::from(*parser.parse(&buf).expect("Parse error")),
    //         )
    //     })
    //     .collect::<Vec<_>>();

    // let sizes = pnfs.iter().map(|(_, pnf)| pnf.size()).collect::<Vec<_>>();

    // let machines = entries
    //     .iter()
    //     .map(|e| {
    //         buf.clear();
    //         let mut name = e.path().file_stem().unwrap().to_os_string();
    //         name.push("-i.scy");
    //         let path = e.path().with_file_name(name);
    //         let mut f = fs::File::open(path).expect("Couldn't open file");

    //         f.read_to_string(&mut buf).expect("Couldn't read file");
    //         serde_yaml::from_str(&buf).expect("Parse error")
    //     })
    //     .collect::<Vec<_>>();

    // let nfas = pnfs
    //     .iter()
    //     .map(|(_, pnf)| {
    //         let start = std::time::Instant::now();
    //         // use the single varable tweak
    //         let nfa = pnf.convert(true, &[]);
    //         let nfa = NFA {
    //             states: nfa.states,
    //             start: nfa.start,
    //             accept: nfa.accept,
    //             transitions: nfa.transitions.iter(),
    //         };
    //         let end = std::time::Instant::now();
    //         let nfa2 = Nfa::from(nfa);
    //         // do the checking
    //         let duration = end.duration_since(start);
    //         (nfa, duration)
    //     })
    //     .collect::<Vec<_>>();

    // // let nfas_single = pnfs
    // //     .iter()
    // //     .map(|(_, pnf)| pnf.convert(true, &[]))
    // //     .collect::<Vec<_>>();
    // // let num_states_single = nfas_single
    // //     .iter()
    // //     .map(|nfa| nfa.states.len())
    // //     .collect::<Vec<_>>();

    // // write to a csv file

    // let mut csv = File::create("size.csv").unwrap();

    // writeln!(csv, "name,size,states,times").unwrap();

    // for ((name, size), (nfa, time)) in entries.iter().zip(sizes.iter()).zip(nfas.iter())
    // // .zip(num_states_single.iter())
    // {
    //     writeln!(
    //         csv,
    //         "{name},{size},{states},{time}",
    //         name = name
    //             .path()
    //             .file_name()
    //             .expect("No file name")
    //             .to_str()
    //             .expect("Invalid unicode"),
    //         states = nfa.states.len(),
    //         time = time.as_millis()
    //     )
    //     .expect("Couldn't write to csv");
    // }
}
