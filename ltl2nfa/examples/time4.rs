use ltl::{
    // karakuri::{self, Karakuri},
    ltl::{PosNormal, FLTL},
    nfa::NFA,
    vata::write_vata,
};
use std::{
    fs::{self, File},
    process::Command,
};

pub fn time(name: &str, ltl: Box<FLTL>) {
    println!("LTL size: {}", ltl.size());
    let pnf = PosNormal::from(*ltl);
    println!("PNF size: {}", pnf.size());
    println!("Number of variables: {}", pnf.vars().len());

    let ltl_nfa = pnf.convert(true, &[]);
    // let ltl_nfa = NFA { // for karakuri
    //     states: ltl_nfa.states.into_iter().map(Some).collect(),
    //     initial: Some(ltl_nfa.initial),
    //     end: ltl_nfa.end.into_iter().map(Some).collect(),
    //     transitions: ltl_nfa
    //         .transitions
    //         .into_iter()
    //         .map(|(q, a, q1)| (Some(q), a, Some(q1)))
    //         .collect(),
    // };
    let ltl_nfa = NFA {
        // for vata
        states: ltl_nfa.states,
        initial: ltl_nfa.initial,
        end: ltl_nfa.end,
        transitions: ltl_nfa
            .transitions
            .into_iter()
            .map(|(q, a, q1)| (q, a.into_iter().next().unwrap(), q1))
            .collect(),
    };

    println!("NFA size: {} states", ltl_nfa.states.len());

    // let nfa_file = fs::File::create(format!("examples/lifts/{name}-scy.txt")).unwrap();

    // let karakuri = Karakuri::from(&ltl_nfa);
    // serde_json::to_writer(nfa_file, &karakuri).unwrap();

    let ltl_file = File::create("ltl.vata").expect("failed to create ltl.vata");
    write_vata(&ltl_nfa, ltl_file).expect("failed to write ltl.vata");

    // let machine_nfa = NFA::from(&machine);

    let machine_file = File::create("machine.vata").expect("failed to create machine.vata");
    write_vata(&ltl_nfa, machine_file).expect("failed to write machine.vata");

    let output = Command::new("./libvata/build/cli/vata")
        .args(["incl", "machine.vata", "ltl.vata"])
        .output()
        .expect("failed to run vata");

    println!("status: {}", output.status);
    println!("stdout: \n{}", String::from_utf8_lossy(&output.stdout));
    println!("stderr: \n{}", String::from_utf8_lossy(&output.stderr));
}

fn main() {
    let mut entries = fs::read_dir("./examples/lifts")
        .expect("couldn't find samples directory")
        .collect::<Result<Vec<_>, _>>()
        .expect("couldn't read entry");

    entries.sort_by_key(|entry| entry.path());

    for entry in entries {
        if entry.path().extension() == Some("ltl".as_ref()) {
            let ltl_file = entry.path();
            println!("{}", ltl_file.display());
            let machine_name = ltl_file.file_stem().unwrap().to_str().unwrap();
            // remove numbers from end
            // machine_name = machine_name.trim_end_matches(|c: char| c.is_digit(10));
            // let mut machine_name = machine_name.to_owned();
            // machine_name += "-i.scy";
            // let machine_file = ltl_file.with_file_name(machine_name);

            let ltl_string = fs::read_to_string(&ltl_file).expect("failed to read ltl file");
            let ltl = ltl::parser::FormulaParser::new()
                .parse(&ltl_string)
                .expect("parse error");

            // let machine_string =
            //     fs::read_to_string(&machine_file).expect("failed to read machine file");
            // let machine = serde_yaml::from_str(&machine_string).expect("parse error");

            let start = std::time::Instant::now();

            time(machine_name, ltl);

            let end = std::time::Instant::now();

            println!("{:5}ms", end.duration_since(start).as_millis());
        }
    }
}
