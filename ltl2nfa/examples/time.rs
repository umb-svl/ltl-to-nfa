use std::fs::{self, File};
use std::io::prelude::*;

use ltl::ltl::PosNormal;
// use plotters::element::BackendCoordOnly;
// use plotters::prelude::*;

fn main() {
    let mut buf = String::new();
    let parser = ltl::parser::FormulaParser::new();

    let entries = fs::read_dir("./samples")
        .expect("Couldn't find samples directory")
        .filter_map(|e| {
            let e = e.unwrap();
            if e.path().extension() == Some("ltl".as_ref()) {
                Some(e)
            } else {
                None
            }
        })
        .collect::<Vec<_>>();
    let pnfs = entries
        .iter()
        .map(|e| {
            buf.clear();
            let mut f = fs::File::open(e.path()).expect("Couldn't open file");
            f.read_to_string(&mut buf).expect("Couldn't read file");
            (
                e,
                PosNormal::from(*parser.parse(&buf).expect("Parse error")),
            )
        })
        .collect::<Vec<_>>();

    let nfas = pnfs
        .iter()
        .map(|(e, pnf)| {
            let name = e.file_name().to_os_string();
            let start = std::time::Instant::now();
            let nfa = pnf.convert(false, &[]);
            let end = std::time::Instant::now();
            let duration = end.duration_since(start);
            (name, nfa, duration)
        })
        .collect::<Vec<_>>();

    let times = nfas.iter().map(|(name, nfa, dur1)| {
        let 
    }).collect::<Vec<_>>();
}
