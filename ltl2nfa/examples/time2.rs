use std::{
    fs::{self, File},
    process::Command,
};

use ltl::{
    karakuri::Karakuri,
    ltl::{PosNormal, FLTL},
    nfa::NFA,
    vata::write_vata,
};

pub fn time(ltl: Box<FLTL>, machine: Karakuri) {
    let pnf = PosNormal::from(*ltl);
    let ltl_nfa = pnf.convert(true, &[]);
    let ltl_nfa = NFA {
        states: ltl_nfa.states,
        initial: ltl_nfa.initial,
        end: ltl_nfa.end,
        transitions: ltl_nfa
            .transitions
            .into_iter()
            .map(|(q, a, q1)| (q, a.into_iter().next().unwrap(), q1))
            .collect(),
    };

    let ltl_file = File::create("ltl.vata").expect("failed to create ltl.vata");
    write_vata(&ltl_nfa, ltl_file).expect("failed to write ltl.vata");

    let machine_nfa = NFA::from(&machine);

    let machine_file = File::create("machine.vata").expect("failed to create machine.vata");
    write_vata(&machine_nfa, machine_file).expect("failed to write machine.vata");

    let output = Command::new("./libvata/build/cli/vata")
        .args(["incl", "machine.vata", "ltl.vata"])
        .output()
        .expect("failed to run vata");

    println!("status: {}", output.status);
    println!("stdout: \n{}", String::from_utf8_lossy(&output.stdout));
    println!("stderr: \n{}", String::from_utf8_lossy(&output.stderr));
}

fn main() {
    for entry in fs::read_dir("./samples").expect("couldn't find samples directory") {
        let entry = entry.expect("couldn't read entry");

        if entry.path().extension() == Some("ltl".as_ref()) {
            println!("{}", entry.path().display());

            let ltl_file = entry.path();
            let mut machine_name = ltl_file.file_stem().unwrap().to_str().unwrap();
            // remove numbers from end
            machine_name = machine_name.trim_end_matches(|c: char| c.is_digit(10));
            let mut machine_name = machine_name.to_owned();
            machine_name += "-i.scy";
            let machine_file = ltl_file.with_file_name(machine_name);

            let ltl_string = fs::read_to_string(&ltl_file).expect("failed to read ltl file");
            let ltl = ltl::parser::FormulaParser::new()
                .parse(&ltl_string)
                .expect("parse error");

            let machine_string =
                fs::read_to_string(&machine_file).expect("failed to read machine file");
            let machine = serde_yaml::from_str(&machine_string).expect("parse error");

            let start = std::time::Instant::now();

            time(ltl, machine);

            let end = std::time::Instant::now();

            println!("{:5}ms", end.duration_since(start).as_millis());
        }
    }
}
