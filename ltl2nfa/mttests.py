tests = '''
(!b.open) W a.open
'''

for i, t in enumerate(tests.strip().split("\n")):
    # write the test to samples/pax_controller{i+!}.ltl
    with open(f"samples/px_controller.ltl", "w") as f:
        f.write(t)