FLTL2NFA
===

This is a tool for converting Finite Linear Temporal Logic (FLTL) formulas to Nondeterministic Finite Automata (NFAs).

Requirements
---

Requires [rust](https://www.rust-lang.org/tools/install) with the nightly channel. To install the nigtly toolchain if it is not already installed, use `rustup toolchain install nightly`.

Usage
---

To acess the REPL, execute `cargo run`. Formulas are input in YAML format as follows:

```
input           LTL formula
Var: A          A
Next: {Var: A}  X a
Until: [p, q]   p U q
Global: p       G p
...
```

The DFA is output in the format accepted by [Gidayu](https://gitlab.com/umb-svl/gidayu).