use std::fmt::{self, Display};
use std::fs;
use std::str::FromStr;

use ariadne::{Color, Label, Report, ReportKind, Source};
use clap::{ArgEnum, Parser};
use lalrpop_util::ParseError;
use ltl::parser::FormulaParser;
use rustyline::error::ReadlineError;
use rustyline::Editor;
use serde::Deserialize;

use ltl::gidayu::Gidayu;
use ltl::karakuri::Karakuri;
use ltl::ltl::PosNormal;

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long)]
    format: Format,
    #[clap(short, long)]
    single: bool,
    #[clap(short, long)]
    eval: Option<String>,
    #[clap(short, long)]
    alphabet: Option<String>,
}

#[derive(Clone, Debug, ArgEnum, Deserialize)]
enum Format {
    Gidayu,
    Karakuri,
}
#[derive(Debug, PartialEq, Eq)]
struct ParseFormatError;
impl Display for ParseFormatError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "invalid output format")
    }
}
impl std::error::Error for ParseFormatError {}
impl FromStr for Format {
    type Err = ParseFormatError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "gidayu" => Ok(Format::Gidayu),
            "karakuri" => Ok(Format::Karakuri),
            _ => Err(ParseFormatError),
        }
    }
}

fn main() {
    let args = Args::parse();
    // eprintln!("{:#?}", args);
    // return;
    let alpha = if let Some(a) = &args.alphabet {
        a.split(",").collect::<Vec<_>>()
    } else {
        vec![]
    };

    if let Some(s) = args.eval {
        match FormulaParser::new().parse(&s) {
            Ok(formula) => {
                // eprintln!("{formula:?}");
                let ltl = PosNormal::from(*formula);
                // eprintln!("{ltl:?}");
                let nfa = ltl.convert(args.single, &alpha).fix_end();
                let json = match args.format {
                    Format::Gidayu => {
                        unimplemented!();
                        // let vars = ltl.vars();
                        // vars.extend(alpha)
                        // let gidayu = Gidayu::from((&alpha, &nfa));
                        // serde_json::to_string(&gidayu).unwrap()
                    }
                    Format::Karakuri => {
                        let karakuri = Karakuri::from(&nfa);
                        serde_yaml::to_string(&karakuri).unwrap()
                    }
                };
                println!("{}", json);
            }
            Err(e) => {
                let span = match e {
                    ParseError::InvalidToken { location, .. } => location..location + 1,
                    ParseError::UnrecognizedEOF { location, .. } => location..location + 1,
                    ParseError::UnrecognizedToken {
                        token: (location, ..),
                        ..
                    } => location..location + 1,
                    ParseError::ExtraToken {
                        token: (start, _, end),
                        ..
                    } => start..end,
                    ParseError::User { .. } => 0..1,
                };
                Report::build(ReportKind::Error, (), 0)
                    .with_message("Parse error")
                    .with_label(
                        Label::new(span)
                            .with_message(e.to_string())
                            .with_color(Color::Red),
                    )
                    .finish()
                    .eprint(Source::from(s))
                    .unwrap();
            }
        }
    } else {
        let mut rl = Editor::<()>::new();
        if rl.load_history("history.txt").is_err() {
            // println!("No previous history.");
        }
        loop {
            match rl.readline(">> ") {
                Ok(line) => {
                    rl.add_history_entry(line.as_str());
                    match FormulaParser::new().parse(&line) {
                        Ok(formula) => {
                            println!("{}", formula.name());
                            let ltl = PosNormal::from(*formula);
                            println!("{}", ltl.name());
                            // println!("{:?}", ltl.vars());
                            // println!("{:?}", ltl.subformulas());
                            let nfa = ltl.convert(args.single, &alpha).fix_end();
                            let json = match args.format {
                                Format::Gidayu => {
                                    let mut vars = ltl.vars();
                                    vars.extend(&alpha);
                                    // vars.remove("END");
                                    let gidayu = Gidayu::from((&vars, &nfa));
                                    let json = serde_yaml::to_string(&gidayu).unwrap();

                                    fs::write("./gidayu.yaml", &json).unwrap();

                                    std::process::Command::new("poetry")
                                        .current_dir("./gidayu")
                                        .arg("run")
                                        .arg(fs::canonicalize("./gidayu/gidayu").unwrap())
                                        .arg("viz")
                                        .arg(fs::canonicalize("./gidayu.yaml").unwrap())
                                        .status()
                                        .unwrap();

                                    json
                                }
                                Format::Karakuri => {
                                    let karakuri = Karakuri::from(&nfa);
                                    serde_yaml::to_string(&karakuri).unwrap()
                                }
                            };
                            println!("{}", json);
                        }
                        Err(e) => {
                            let span = match e {
                                ParseError::InvalidToken { location, .. } => location..location + 1,
                                ParseError::UnrecognizedEOF { location, .. } => {
                                    location..location + 1
                                }
                                ParseError::UnrecognizedToken {
                                    token: (location, ..),
                                    ..
                                } => location..location + 1,
                                ParseError::ExtraToken {
                                    token: (start, _, end),
                                    ..
                                } => start..end,
                                ParseError::User { .. } => 0..1,
                            };
                            Report::build(ReportKind::Error, (), 0)
                                .with_message("Parse error")
                                .with_label(
                                    Label::new(span)
                                        .with_message(e.to_string())
                                        .with_color(Color::Red),
                                )
                                .finish()
                                .print(Source::from(line))
                                .unwrap();
                        }
                    }
                }
                Err(ReadlineError::Interrupted) => {
                    println!("CTRL-C");
                    break;
                }
                Err(ReadlineError::Eof) => {
                    println!("CTRL-D");
                    break;
                }
                Err(err) => {
                    println!("Error: {:?}", err);
                    break;
                }
            }
        }
        rl.save_history("history.txt").unwrap();
    }
}
