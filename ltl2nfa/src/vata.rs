use std::fmt::Display;
use std::hash::Hash;
use std::io::{self, Write};

use ahash::{AHashMap, AHashSet};

use crate::nfa::NFA;

/// Write an automaton for LIBVATA
pub fn write_vata<A, S, W>(nfa: &NFA<A, S>, mut out: W) -> io::Result<()>
where
    A: Hash + Eq + Display,
    S: Hash + Eq,
    W: Write,
{
    let mut states = AHashMap::default();
    for state in &nfa.states {
        states.insert(state, states.len());
    }

    write!(out, "Ops")?;
    write!(out, " START:0")?;
    for a in nfa
        .transitions
        .iter()
        .map(|(_, a, _)| a)
        .collect::<AHashSet<_>>()
    {
        write!(out, " {}:1", a)?;
    }
    writeln!(out)?;

    writeln!(out, "Automaton A")?;
    write!(out, "States")?;
    // for (_, n) in &states { // apparently we dont need these
    //     write!(out, " q{}", n)?;
    // }
    writeln!(out)?;

    write!(out, "Final States")?;
    for s in &nfa.end {
        write!(out, " q{}", states[s])?;
    }
    writeln!(out)?;

    writeln!(out, "Transitions")?;
    writeln!(out, "START() -> {}", states[&nfa.initial])?;
    for (q, pi, q1) in &nfa.transitions {
        writeln!(out, "{}(q{}) -> q{}", pi, states[q], states[q1])?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vata() {
        let nfa = NFA {
            states: AHashSet::from_iter([1, 2, 3]),
            initial: 1,
            end: AHashSet::from_iter([3]),
            transitions: AHashSet::from_iter([(1, 'a', 2), (2, 'b', 3), (3, 'c', 3)]),
        };

        let mut out = Vec::new();
        write_vata(&nfa, &mut out).unwrap();

        let out = String::from_utf8(out).unwrap();
        assert_eq!(
            out,
            "Ops a:1 b:1 c:1\n\
            Automaton A\n\
            States q0 q1 q2\n\
            Final States q2\n\
            Transitions\n\
            a(q0) -> q1\n\
            b(q1) -> q2\n\
            c(q2) -> q2\n"
        );
    }
}
