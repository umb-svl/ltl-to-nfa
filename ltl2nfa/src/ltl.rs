use std::collections::BTreeSet;
use std::fmt::{self, Display};
use std::hash::Hash;

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub enum FLTL {
    True,
    Var(String),
    Not(Box<FLTL>),
    And(Box<FLTL>, Box<FLTL>),
    Next(Box<FLTL>),
    Until(Box<FLTL>, Box<FLTL>),
}
impl FLTL {
    pub fn tru() -> Box<Self> {
        FLTL::True.into()
    }
    pub fn fals() -> Box<Self> {
        FLTL::not(FLTL::tru()).into()
    }
    pub fn var(name: String) -> Box<Self> {
        FLTL::Var(name).into()
    }
    pub fn not(f: Box<Self>) -> Box<Self> {
        if let FLTL::Not(f) = *f {
            return f;
        } else {
            FLTL::Not(f).into()
        }
    }
    pub fn and(f1: Box<Self>, f2: Box<Self>) -> Box<Self> {
        FLTL::And(f1, f2).into()
    }
    pub fn or(f1: Box<Self>, f2: Box<Self>) -> Box<Self> {
        FLTL::not(FLTL::and(FLTL::not(f1), FLTL::not(f2))).into()
    }
    pub fn implies(f1: Box<Self>, f2: Box<Self>) -> Box<Self> {
        FLTL::or(FLTL::not(f1), f2).into()
    }
    pub fn iff(f1: Box<Self>, f2: Box<Self>) -> Box<Self> {
        FLTL::and(FLTL::implies(f1.clone(), f2.clone()), FLTL::implies(f2, f1)).into()
    }
    pub fn next(f: Box<Self>) -> Box<Self> {
        FLTL::Next(f).into()
    }
    pub fn until(f1: Box<Self>, f2: Box<Self>) -> Box<Self> {
        FLTL::Until(f1, f2).into()
    }
    pub fn release(f1: Box<Self>, f2: Box<Self>) -> Box<Self> {
        FLTL::not(FLTL::until(FLTL::not(f1), FLTL::not(f2)))
    }
    pub fn finally(f: Box<Self>) -> Box<Self> {
        FLTL::until(FLTL::tru(), f)
    }
    pub fn globally(f: Box<Self>) -> Box<Self> {
        FLTL::release(FLTL::fals(), f)
    }
    pub fn name(&self) -> impl Display + '_ {
        struct Name<'a>(&'a FLTL);
        impl Display for Name<'_> {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                match self.0 {
                    FLTL::True => write!(f, "true"),
                    FLTL::Var(a) => write!(f, "{}", a),
                    FLTL::Not(p) => write!(f, "(! {})", p.name()),
                    FLTL::And(p, q) => write!(f, "({} & {})", p.name(), q.name()),
                    FLTL::Next(p) => write!(f, "(X {})", p.name()),
                    FLTL::Until(p, q) => write!(f, "({} U {})", p.name(), q.name()),
                }
            }
        }
        Name(self)
    }
    pub fn size(&self) -> usize {
        match self {
            FLTL::True | FLTL::Var(_) => 1,
            FLTL::Not(p) | FLTL::Next(p) => 1 + p.size(),
            FLTL::And(p, q) | FLTL::Until(p, q) => 1 + p.size() + q.size(),
        }
    }
}

impl From<FLTL> for PosNormal {
    fn from(ltl: FLTL) -> Self {
        fn from_neg(p: Box<FLTL>) -> PosNormal {
            match *p {
                FLTL::True => PosNormal::False,
                FLTL::Var(n) => PosNormal::NegVar(n),
                FLTL::Not(p) => p.into(),
                FLTL::And(p, q) => PosNormal::Or(Box::new(from_neg(p)), Box::new(from_neg(q))),
                FLTL::Next(p) => PosNormal::WeakNext(Box::new(from_neg(p))),
                FLTL::Until(p, q) => {
                    PosNormal::Release(Box::new(from_neg(p)), Box::new(from_neg(q)))
                }
            }
        }
        match ltl {
            FLTL::True => PosNormal::True,
            FLTL::Var(n) => PosNormal::Var(n),
            FLTL::Not(p) => from_neg(p),
            FLTL::And(p, q) => PosNormal::And(Box::new(p.into()), Box::new(q.into())),
            FLTL::Next(p) => PosNormal::Next(Box::new(p.into())),
            FLTL::Until(p, q) => PosNormal::Until(Box::new(p.into()), Box::new(q.into())),
        }
    }
}
impl From<Box<FLTL>> for PosNormal {
    fn from(ltl: Box<FLTL>) -> Self {
        (*ltl).into()
    }
}

/// A FLTL formula in positive normal form.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub enum PosNormal {
    True,
    False,
    Var(String),
    NegVar(String),
    And(Box<PosNormal>, Box<PosNormal>),
    Or(Box<PosNormal>, Box<PosNormal>),
    Next(Box<PosNormal>),
    Until(Box<PosNormal>, Box<PosNormal>),
    WeakNext(Box<PosNormal>),
    Release(Box<PosNormal>, Box<PosNormal>),
}

// mod serde_var

impl PosNormal {
    pub fn name(&self) -> impl Display + '_ {
        struct Name<'a>(&'a PosNormal);
        impl Display for Name<'_> {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                match self.0 {
                    PosNormal::True => write!(f, "true"),
                    PosNormal::False => write!(f, "false"),
                    PosNormal::Var(a) => write!(f, "{}", a),
                    PosNormal::NegVar(a) => write!(f, "(! {})", a),
                    PosNormal::And(p, q) => write!(f, "({} & {})", p.name(), q.name()),
                    PosNormal::Or(p, q) => write!(f, "({} | {})", p.name(), q.name()),
                    PosNormal::Next(p) => write!(f, "(X {})", p.name()),
                    // PosNormal::Eventual(p) => write!(f, "e{}", p.name()),
                    PosNormal::Until(p, q) => write!(f, "({} U {})", p.name(), q.name()),
                    PosNormal::WeakNext(p) => write!(f, "(WX {})", p.name()),
                    // PosNormal::Global(p) => write!(f, "g{}", p.name()),
                    PosNormal::Release(p, q) => write!(f, "({} R {})", p.name(), q.name()),
                }
            }
        }
        Name(self)
    }
    pub fn latex(&self) -> impl Display + Copy + '_ {
        #[derive(Clone, Copy)]
        struct Name<'a>(&'a PosNormal);
        impl Display for Name<'_> {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                match self.0 {
                    PosNormal::True => write!(f, "\\text{{true}}"),
                    PosNormal::False => write!(f, "\\text{{false}}"),
                    PosNormal::Var(a) => write!(f, "\\text{{{}}}", a),
                    PosNormal::NegVar(a) => write!(f, "\\neg \\text{{{}}}", a),
                    _ => {
                        if f.alternate() {
                            write!(f, "(")?;
                        }
                        match self.0 {
                            PosNormal::True
                            | PosNormal::False
                            | PosNormal::Var(_)
                            | PosNormal::NegVar(_) => unreachable!(),
                            PosNormal::And(p, q) => {
                                write!(f, "{:#} \\wedge {:#}", p.latex(), q.latex())
                            }
                            PosNormal::Or(p, q) => {
                                write!(f, "{:#} \\vee {:#}", p.latex(), q.latex())
                            }
                            PosNormal::Next(p) => write!(f, "\\operatorname{{X}} {:#}", p.latex()),
                            // PosNormal::Eventual(p) => {
                            //     write!(f, "\\operatorname{{E}} {:#}", p.latex())
                            // }
                            PosNormal::Until(p, q) => {
                                write!(
                                    f,
                                    "{:#} \\operatorname{{\\mathcal U}} {:#}",
                                    p.latex(),
                                    q.latex()
                                )
                            }
                            PosNormal::WeakNext(p) => {
                                write!(f, "\\operatorname{{W}} {:#}", p.latex())
                            }
                            // PosNormal::Global(p) => write!(f, "\\operatorname{{G}}{:#}", p.latex()),
                            PosNormal::Release(p, q) => {
                                write!(
                                    f,
                                    "{:#} \\operatorname{{\\mathcal R}} {:#}",
                                    p.latex(),
                                    q.latex()
                                )
                            }
                        }?;
                        if f.alternate() {
                            write!(f, ")")?;
                        }
                        Ok(())
                    }
                }
            }
        }
        Name(self)
    }
    pub fn vars(&self) -> BTreeSet<&str> {
        let mut set = BTreeSet::new();
        self.vars_into(&mut set);
        set
    }
    fn vars_into<'a>(&'a self, set: &mut BTreeSet<&'a str>) {
        match self {
            PosNormal::True | PosNormal::False => {}
            PosNormal::Var(x) | PosNormal::NegVar(x) => {
                if x == "END" {
                    panic!("variables can't be named `END`");
                }
                set.insert(x);
            }
            PosNormal::Next(p) => {
                p.vars_into(set);
            }
            PosNormal::WeakNext(p) => {
                set.insert("END");
                p.vars_into(set);
            }
            PosNormal::Until(p, q) | PosNormal::And(p, q) | PosNormal::Or(p, q) => {
                p.vars_into(set);
                q.vars_into(set);
            }
            PosNormal::Release(p, q) => {
                set.insert("END");
                p.vars_into(set);
                q.vars_into(set);
            }
        }
    }
    pub fn subformulas(&self) -> BTreeSet<&PosNormal> {
        let mut set = Default::default();
        self.subformulas_into(&mut set);
        set
    }
    fn subformulas_into<'a>(&'a self, set: &mut BTreeSet<&'a PosNormal>) {
        set.insert(self);
        match self {
            PosNormal::Next(p) | PosNormal::WeakNext(p) => {
                p.subformulas_into(set);
            }
            PosNormal::Until(p, q)
            | PosNormal::And(p, q)
            | PosNormal::Or(p, q)
            | PosNormal::Release(p, q) => {
                p.subformulas_into(set);
                q.subformulas_into(set)
            }
            _ => {}
        }
    }

    pub fn num_vars(&self) -> usize {
        self.vars().len()
    }
    /// The number of distinct subformulas.
    pub fn num_subformulas(&self) -> usize {
        self.subformulas().len()
    }
    /// The size of the syntax tree
    pub fn size(&self) -> usize {
        match self {
            PosNormal::True | PosNormal::False => 1,
            PosNormal::Var(_) | PosNormal::NegVar(_) => 1,
            PosNormal::Next(p) | PosNormal::WeakNext(p) => 1 + p.size(),
            PosNormal::Until(p, q)
            | PosNormal::And(p, q)
            | PosNormal::Or(p, q)
            | PosNormal::Release(p, q) => 1 + p.size() + q.size(),
        }
    }
}
