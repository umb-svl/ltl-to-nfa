use std::hash::Hash;

use ahash::AHashMap;
use automata::{nfa::Nfa, Alphabet};

use crate::nfa::NFA;

// convert for the `automata` library
impl<A, S> From<&NFA<A, S>> for Nfa<A>
where
    A: Alphabet,
    S: Hash + Eq,
{
    fn from(nfa: &NFA<A, S>) -> Self {
        let mut states = AHashMap::new();
        states.insert(&nfa.initial, 0);
        for state in &nfa.states {
            if !states.contains_key(state) {
                states.insert(state, states.len());
            }
        }
        Nfa::from_edges(
            nfa.transitions
                .iter()
                .map(|(q, a, q2)| (states[q], Some(*a), states[q2])),
            nfa.end.iter().map(|q| states[q]),
        )
    }
}
