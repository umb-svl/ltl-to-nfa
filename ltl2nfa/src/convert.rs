use std::collections::BTreeSet;
use std::iter;
use std::mem::swap;
use std::ops::BitOr;

use ahash::AHashSet;
use itertools::Itertools;

use crate::ltl::PosNormal;
use crate::nfa::NFA;

fn pset<S, T>(set: S) -> T
where
    S: IntoIterator + FromIterator<S::Item>,
    S::Item: Clone,
    T: FromIterator<S>,
{
    set.into_iter()
        .powerset()
        .map(FromIterator::from_iter)
        .collect()
}
fn or<S>(mut a: Vec<S>, b: Vec<S>) -> Vec<S> {
    a.extend(b);
    a
}
fn and<S>(a: Vec<S>, b: Vec<S>) -> Vec<S>
where
    for<'a> &'a S: BitOr<Output = S>,
{
    let mut v = vec![];
    for x in &a {
        for y in &b {
            v.push(x | y);
        }
    }
    v
}

fn minimize<T>(mut a: Vec<BTreeSet<T>>) -> Vec<BTreeSet<T>>
where
    T: Ord,
{
    let mut res: Vec<BTreeSet<_>> = Vec::with_capacity(a.len());
    a.sort_unstable_by_key(|s| s.len());
    for s in a {
        if res.iter().any(|r| r.is_subset(&s)) {
            continue;
        }
        res.push(s);
    }

    res

    // a // replace w no-op
}

impl PosNormal {
    // generates minimal satisfying sets (kind of)
    fn delta_min<'a>(self: &'a PosNormal, pi: &BTreeSet<&str>) -> Vec<BTreeSet<&'a PosNormal>> {
        // helper function to deal with lifetime issues -- conceptually the same as Next(phi).delta_min(pi)
        fn delta_min_next<'a>(
            weak: bool,
            phi: &'a PosNormal,
            pi: &BTreeSet<&str>,
        ) -> Vec<BTreeSet<&'a PosNormal>> {
            if weak && pi.contains("END") {
                vec![BTreeSet::new()]
            } else {
                vec![BTreeSet::from_iter([phi])]
            }
        }
        match self {
            PosNormal::True => vec![BTreeSet::new()],
            PosNormal::False => vec![],
            PosNormal::Var(a) => {
                if pi.contains(a.as_str()) {
                    vec![BTreeSet::new()] // true
                } else {
                    vec![] // false
                }
            }
            PosNormal::NegVar(a) => {
                if pi.contains(a.as_str()) {
                    vec![]
                } else {
                    vec![BTreeSet::new()]
                }
            }
            PosNormal::And(p, q) => and(p.delta_min(pi), q.delta_min(pi)),
            PosNormal::Or(p, q) => or(p.delta_min(pi), q.delta_min(pi)),

            PosNormal::Next(p) => delta_min_next(false, p, pi),
            // PosNormal::Eventual(p) => or(p.delta_min(pi), delta_min_next(false, self, pi)),
            PosNormal::Until(p, q) => or(
                q.delta_min(pi),
                and(p.delta_min(pi), delta_min_next(false, self, pi)),
            ),

            PosNormal::WeakNext(p) => delta_min_next(true, p, pi),
            // PosNormal::Global(p) => and(p.delta_min(pi), delta_min_next(true, self, pi)),
            PosNormal::Release(p, q) => and(
                q.delta_min(pi),
                or(p.delta_min(pi), delta_min_next(true, self, pi)),
            ),
        }
    }
    pub fn convert<'a>(
        &'a self,
        single: bool,
        alpha: &[&'a str],
    ) -> NFA<BTreeSet<&'a str>, BTreeSet<&PosNormal>> {
        let mut vars = self.vars();
        vars.extend(alpha);
        // let alpha = alpha.map_or(self.vars(), |a| a.iter().map(|s| *s).collect());
        let letters: BTreeSet<_> = if single {
            if vars.contains("END") {
                vars.into_iter()
                    .flat_map(|v| {
                        if v == "END" {
                            vec![]
                        } else {
                            vec![BTreeSet::from_iter([v]), BTreeSet::from_iter(["END", v])]
                        }
                    })
                    .collect()
            } else {
                vars.into_iter().map(|v| BTreeSet::from_iter([v])).collect()
            }
        } else {
            // vars.insert("END");
            pset(vars)
        };
        let initial = BTreeSet::from_iter([self]);
        let end = BTreeSet::default();
        let mut states = AHashSet::from_iter([initial.clone()]);
        let mut transitions = AHashSet::default();
        let mut new_states = vec![];
        let mut fresh_states: Vec<BTreeSet<&PosNormal>> = vec![initial.clone()];
        loop {
            let mut changed = false;
            for q in &fresh_states {
                for pi in &letters {
                    let minsets = minimize(
                        q.into_iter()
                            .fold(vec![BTreeSet::new()], |ms, phi| and(ms, phi.delta_min(pi))),
                    );
                    // println!("minsets({:?}, {:?}) = {:?}", q, pi, minsets);
                    for qprime in minsets {
                        new_states.push(qprime.clone());
                        changed |= transitions.insert((q.clone(), pi.clone(), qprime.clone()));
                    }
                }
            }
            if !changed {
                break;
            }
            // dbg!(&fresh_states);
            // dbg!(&new_states);

            // this only keeps states that were not already there
            new_states.retain(|s| states.insert(s.clone()));
            // println!("added {} states", new_states.len());
            fresh_states.clear();
            swap(&mut new_states, &mut fresh_states)
        }
        NFA {
            states,
            initial,
            end: [end].into_iter().collect(),
            transitions,
        }
    }
}

impl<'a, 'b> NFA<BTreeSet<&'a str>, BTreeSet<&'b PosNormal>> {
    pub fn fix_end(self) -> NFA<BTreeSet<&'a str>, Option<BTreeSet<&'b PosNormal>>> {
        let states = self
            .states
            .into_iter()
            .map(Some)
            .chain(
                if self
                    .transitions
                    .iter()
                    .find(|(_, pi, _)| pi.contains("END"))
                    .is_some()
                {
                    Some(None)
                } else {
                    None
                },
            )
            .collect();
        let transitions = self
            .transitions
            .into_iter()
            .filter_map(|(q, mut pi, q2)| {
                if pi.contains("END") {
                    if q2.is_empty() {
                        pi.remove("END");
                        Some((Some(q), pi, None))
                    } else {
                        None
                    }
                } else {
                    Some((Some(q), pi, Some(q2)))
                }
            })
            .collect();

        NFA {
            states,
            initial: Some(self.initial),
            end: self
                .end
                .into_iter()
                .map(Some)
                .chain(iter::once(None))
                .collect(),
            transitions,
        }
    }
}

impl<'a> From<&'a PosNormal> for NFA<BTreeSet<&'a str>, BTreeSet<&'a PosNormal>> {
    fn from(ltl: &'a PosNormal) -> Self {
        ltl.convert(false, &[])
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[bench]
//     fn bench_conversion(bench: &mut test::Bencher) {
//         let ltl = PosNormal::Until(
//             Box::new(PosNormal::Var("A")),
//             Box::new(PosNormal::Until(PosNormal::Var("B"), PosNormal::Var("C"))),
//         );
//         b.iter(|| {})
//     }
// }
