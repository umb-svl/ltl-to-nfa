#![feature(iter_intersperse)]
// #![cfg_attr(test, feature(test))]

use lalrpop_util::lalrpop_mod;

pub mod automata;
pub mod convert;
pub mod gidayu;
pub mod karakuri;
pub mod ltl;
pub mod nfa;
pub mod vata;
lalrpop_mod!(pub parser, "/ltl.rs");
