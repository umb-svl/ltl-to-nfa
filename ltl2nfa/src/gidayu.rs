use std::collections::{BTreeMap, BTreeSet};
use std::ops::Not;

use ahash::AHashMap;
use serde::Serialize;

use crate::ltl::PosNormal;
use crate::nfa::NFA;

#[derive(Debug, Serialize)]
pub struct Gidayu {
    states: AHashMap<String, State>,
    transitions: Vec<Transition>,
}

#[derive(Debug, Serialize)]
pub struct State {
    label: String,
    #[serde(skip_serializing_if = "Not::not")]
    initial: bool,
    #[serde(skip_serializing_if = "Not::not")]
    r#final: bool,
}

#[derive(Debug, Serialize)]
pub struct Transition {
    chars: Vec<String>,
    src: String,
    dst: String,
}

impl
    From<(
        &BTreeSet<&str>,
        &NFA<BTreeSet<&str>, Option<BTreeSet<&PosNormal>>>,
    )> for Gidayu
{
    fn from(
        (alpha, nfa): (
            &BTreeSet<&str>,
            &NFA<BTreeSet<&str>, Option<BTreeSet<&PosNormal>>>,
        ),
    ) -> Self {
        let names = nfa
            .states
            .iter()
            .map(|q| {
                let name = if let Some(q) = q {
                    if q.is_empty() {
                        "\\emptyset".to_string()
                    } else {
                        format!(
                            "\\{{{}\\}}",
                            q.into_iter()
                                .map(|phi| phi.latex().to_string())
                                .intersperse(", ".to_string())
                                .collect::<String>()
                        )
                    }
                } else {
                    "\\operatorname{FINAL}".to_string()
                };
                (q, name)
            })
            .collect::<AHashMap<_, _>>();
        let mut transitions: AHashMap<(_, _), Vec<_>> = AHashMap::new();
        for (q, pi, qprime) in &nfa.transitions {
            if transitions.contains_key(&(q, qprime)) {
                transitions.get_mut(&(q, qprime)).unwrap().push(pi)
            } else {
                transitions.insert((q, qprime), vec![pi]);
            }
        }

        let char_map = BTreeMap::from_iter(alpha.iter().map(|ch| (ch, false)));

        Gidayu {
            states: names
                .iter()
                .map(|(&state, name)| {
                    let initial = state == &nfa.initial;
                    let r#final = nfa.end.contains(state);
                    (
                        name.clone(),
                        State {
                            label: name.clone(),
                            initial,
                            r#final,
                        },
                    )
                })
                .collect(),
            transitions: transitions
                .into_iter()
                .map(|((q, qprime), pi)| Transition {
                    chars: pi
                        .into_iter()
                        .map(|cs| {
                            let mut map = char_map.clone();
                            for c in cs {
                                map.insert(c, true);
                            }
                            map.iter()
                                .map(|(c, &b)| {
                                    if b {
                                        format!("{c} = \\top",)
                                    } else {
                                        format!("\\textcolor{{lightgray}}{{{c} = \\bot}}")
                                    }
                                })
                                .intersperse(", ".to_string())
                                .collect::<String>()
                        })
                        .collect(),
                    src: names.get(&q).unwrap().clone(),
                    dst: names.get(&qprime).unwrap().clone(),
                })
                .collect(),
        }
    }
}
