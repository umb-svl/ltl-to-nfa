use std::collections::BTreeSet;

use ahash::{AHashMap, AHashSet};
use serde::{Deserialize, Serialize};

use crate::ltl::PosNormal;
use crate::nfa::NFA;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Karakuri {
    start_state: String,
    accepted_states: Vec<String>,
    edges: Vec<Edge>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
struct Edge {
    src: String,
    dst: String,
    char: String,
}

impl<'a> From<&'a Karakuri> for NFA<&'a str, &'a str> {
    fn from(karakuri: &'a Karakuri) -> Self {
        let initial = &karakuri.start_state;
        let end = karakuri.accepted_states.iter().map(|s| &s[..]).collect();
        let mut states = AHashSet::new();
        let transitions = karakuri
            .edges
            .iter()
            .map(|edge| {
                let src = &edge.src[..];
                let dst = &edge.dst[..];
                let char = &edge.char[..];
                states.insert(src);
                states.insert(dst);
                (src, char, dst)
            })
            .collect();
        Self {
            states,
            initial,
            end,
            transitions,
        }
    }
}

impl From<&NFA<BTreeSet<&str>, Option<BTreeSet<&PosNormal>>>> for Karakuri {
    fn from(nfa: &NFA<BTreeSet<&str>, Option<BTreeSet<&PosNormal>>>) -> Self {
        let names = nfa
            .states
            .iter()
            .map(|q| {
                let name = if let Some(q) = q {
                    q.into_iter()
                        .map(|phi| phi.name().to_string())
                        .intersperse(",".to_string())
                        .collect::<String>()
                } else {
                    "FINAL".to_string()
                };
                (q, name)
            })
            .chain(std::iter::once((&None, "FINAL".to_string())))
            .collect::<AHashMap<_, _>>();

        let start_state = names.get(&nfa.initial).unwrap().clone();
        let accepted_states = nfa
            .end
            .iter()
            .map(|s| names.get(s).unwrap().clone())
            .collect();
        let edges = nfa
            .transitions
            .iter()
            .map(|(q, pi, qprime)| {
                let src = names.get(q).unwrap().clone();
                let dst = names.get(qprime).unwrap().clone();
                let char = pi
                    .into_iter()
                    .map(|c| *c)
                    .intersperse("_")
                    .collect::<String>();
                Edge { src, dst, char }
            })
            .collect::<Vec<_>>();
        Karakuri {
            start_state: start_state,
            accepted_states,
            edges,
        }
    }
}
