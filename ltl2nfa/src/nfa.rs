use std::hash::Hash;

use ahash::AHashSet;
use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct NFA<ALPHA: Hash + Eq, STATE: Hash + Eq> {
    pub states: AHashSet<STATE>,
    pub initial: STATE,
    pub end: AHashSet<STATE>,
    pub transitions: AHashSet<(STATE, ALPHA, STATE)>,
}
