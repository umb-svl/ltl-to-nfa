#!/usr/bin/env python3
import subprocess
from pathlib import Path    
from karakuri.karakuri.regular import NFA, DFA, nfa_to_dfa
import yaml
import pprint
import sys

DEBUG = False

def pcolor(b: bool):
    if b:
        return "\033[92msuccess ✔\033[00m"
    else: 
        return "\033[91mfailed ✘\033[00m"

def check(name: str, ltl: str, machine: dict):
    machine_nfa = NFA.from_dict(machine)
    machine_dfa = nfa_to_dfa(machine_nfa)
    nil = DFA.make_nil(machine_nfa.alphabet)
    machine_dfa = machine_dfa.subtract(nil)

    ltl_nfa = subprocess.run([
        "cargo",
        "run",
        "--quiet",
        "--release",
        "--",
        "--format",
        "karakuri",
        "--single",
        "--eval",
        ltl,
        "--alphabet",
        ",".join(machine_dfa.alphabet),
    ],   
        stdout=subprocess.PIPE,
    ).stdout
    new_var = yaml.safe_load(ltl_nfa)
    ltl_nfa = NFA.from_dict(new_var)

    ltl_dfa = nfa_to_dfa(ltl_nfa)

    if DEBUG:
        print(f"{name} ltl:")
        pprint.pp(ltl_dfa.as_dict())
        print(f"{name} machine:")
        pprint.pp(machine_dfa.as_dict())

    print(f"{name} ltl contains machine: {pcolor(ltl_dfa.contains(machine_dfa))}")
    if DEBUG:
        print(f"difference is:")
        pprint.pp(machine_dfa.subtract(ltl_dfa).as_dict())
    if DEBUG:
        print(f"{name} machine contains ltl: {machine_dfa.contains(ltl_dfa)}")
        print("difference is:")
        pprint.pp(ltl_dfa.subtract(machine_dfa).as_dict())


def main():
    for path in Path("./samples").glob("*.ltl"):
        # path = Path("./samples/repeat.ltl")
        with open(path) as f:
            ltl = f.read()
        with open(f"./samples/{path.stem.rstrip('1234567890')}-i.scy") as f:
            machine = yaml.safe_load(f)
        print(f"running {path.stem}:\n    {ltl}")
        check(path.stem, ltl, machine)


if __name__ == "__main__":
    if "DEBUG" in sys.argv:
        DEBUG = True
    main()