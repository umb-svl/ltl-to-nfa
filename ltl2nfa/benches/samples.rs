#![feature(test)]

use std::fs::{self, File};
use std::io::prelude::*;

use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use ltl::ltl::PosNormal;
use ltl::parser::FormulaParser;
use pprof::criterion::{Output, PProfProfiler};

fn bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("Standalone benches");
    let mut buf = String::new();
    let parser = FormulaParser::new();
    let entries = fs::read_dir("./samples")
        .unwrap()
        .filter_map(|e| {
            let e = e.unwrap();
            if e.path().extension() == Some("ltl".as_ref()) {
                Some(e)
            } else {
                None
            }
        })
        .collect::<Vec<_>>();
    let pnfs = entries
        .iter()
        .map(|e| {
            buf.clear();
            let mut f = File::open(e.path()).unwrap();
            f.read_to_string(&mut buf).unwrap();
            (e, PosNormal::from(*parser.parse(&buf).unwrap()))
        })
        .collect::<Vec<_>>();
    for (e, pnf) in &pnfs {
        let size = pnf.size();
        group.bench_with_input(
            BenchmarkId::new(e.file_name().to_str().unwrap(), size),
            &size,
            |b, _| b.iter_with_large_drop(|| pnf.convert(false, &[])),
        );
    }
}

criterion_group! {
    name = samples;
    config = Criterion::default().with_profiler(PProfProfiler::new(100, Output::Flamegraph(None)));
    targets = bench
}
criterion_main!(samples);
